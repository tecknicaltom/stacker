#!/bin/bash
set -eu

if [[ $# != 3 ]]
then
	echo Usage: $0 git_repo_url git_username git_repo_access_token
	exit 1
fi

if [[ -e /etc/os-release && $(grep '^ID=' /etc/os-release) == "ID=arch" ]]
then
	# install docker
	pacman --noconfirm -S docker docker-buildx docker-compose
	systemctl enable docker.service
	systemctl start docker.service
else
	echo Unsupported system
	exit 1
fi

REPO_URL=$1
USERNAME=$2
ACCESS_TOKEN=$3

# init swarm mode
docker swarm init

# create secret for git creds
echo "https://${USERNAME}:${ACCESS_TOKEN}@gitlab.com" | docker secret create stacker_git_credentials -

# create config
echo "STACKER_REPO=${REPO_URL}" | docker config create stacker_config -

# launch stacker
curl -s https://gitlab.com/tecknicaltom/stacker/-/raw/master/docker-compose.yml | docker stack deploy --compose-file - stacker
