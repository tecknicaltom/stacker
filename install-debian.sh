#!/bin/bash
set -eu

# install docker
apt update
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
apt update
apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

# init swarm mode
docker swarm init

# create secret for git creds
echo 'https://[USERNAME]:[REPO_ACCESS_TOKEN]@gitlab.com' | docker secret create stacker_git_credentials -

# create config
echo 'STACKER_REPO=https://gitlab.com/USERNAME/MYSTACKER.git' | docker config create stacker_config -

# launch stacker
curl -s https://gitlab.com/tecknicaltom/stacker/-/raw/master/docker-compose.yml | docker stack deploy --compose-file - stacker
