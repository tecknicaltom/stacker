#!/bin/bash
set -eu

DEFAULT_SLEEP=60

. /stacker_config

ln -s /run/secrets/stacker_git_credentials ~/.git-credentials
git config --global credential.helper store

if [[ ! -e config_repo ]]
then
	git clone "$STACKER_REPO" config_repo
fi
cd config_repo

while :
do
	if [[ -e stacker_config ]]
	then
		. ./stacker_config
	fi

	for project in *
	do
		if [[ -d "$project" ]]
		then
			pushd "$project"
			if [[ -e docker-compose.yml ]]
			then
				docker stack deploy --compose-file docker-compose.yml "$project"
			fi
			popd
		fi
	done

	sleep ${STACKER_SLEEP:-$DEFAULT_SLEEP}
	git pull
done
