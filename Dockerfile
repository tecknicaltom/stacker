FROM debian

RUN \
    apt update && \
    apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg \
        lsb-release \
    && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg && \
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list && \
    apt update && \
    apt install -y docker-ce docker-ce-cli containerd.io && \
    rm -rf /var/lib/apt/lists/* && \
    apt clean

WORKDIR /app
COPY main.sh /app/main.sh
CMD /app/main.sh
